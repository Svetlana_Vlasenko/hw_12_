<?php
class Product{
    public $title ="Название обьекта";
    public $type ="Тип";
    public $address = "Адресс";
    public $price =0;
    public $description = "Описание";


    public function __construct($title,$type,$address,$price,$description){
        $this->title = $title;
        $this->type = $type;
        $this->address = $address;
        $this->price = $price;
        $this->description = $description;
    }
    
    public function getSummaryLine(){
        return $this->title
          . ' ' . $this->type
          . ' ' . $this->address
          . ' ' . $this->price;
    }

}

?>