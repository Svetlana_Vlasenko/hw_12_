<?php
include_once $_SERVER['DOCUMENT_ROOT'].'/shop/index.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/templates/header_connect.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/templates/header.php';?>
<div class="container">
    <div class="row">
        <?php foreach($productsObjects as  $value):?>
            <div class=" col-4">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title"><?=$value->title?></h5>
                        <p class="card-text"><?=$value->type?></p>
                        <p class="card-text"><?=$value->price?></p>
                          <a href="/two.php?title=<?=$value->title?>">More details</a>  
                    </div>
                </div>    
            </div>
        <?php endforeach;?>
    </div>
</div>     
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/templates/footer.php';?>