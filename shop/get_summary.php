<?php include_once $_SERVER['DOCUMENT_ROOT'].'/shop/index.php';?>
<div class="content">
    <div class="row d-flex justify-content-center">
        <div class="col-6 ">
            <h1>The Property</h1>
            <ul class="list-group">
                <?php foreach($productsObjects as $value):?>
                    <li class="list-group-item">
                        <?=$value->getSummaryLine()?>
                    </li>
                <?php endforeach;?>
            </ul>
        </div>
    </div>
</div>