<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/classes/Product.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/classes/House.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/classes/HotelRoom.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/classes/Apartment.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/classes/HtmlProductsWriter.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/data/products.php';

$productsObjects = [];
foreach($products as $productArray){
  if($productArray['type'] === 'hotel_room'){
    $productsObjects[] = new HotelRoom($productArray['title'],
    $productArray['type'],
      $productArray['address'],
      $productArray['price'],
      $productArray['description'],
      $productArray['roomNumber']
    );
  }elseif($productArray['type'] === 'house'){
    $productsObjects[] = new House($productArray['title'],
    $productArray['type'],
      $productArray['address'],
      $productArray['price'],
      $productArray['description'],
      $productArray['roomsAmount']
    );
  }else($productArray['type'] === 'apartment'){
    $productsObjects[] = new Apartment($productArray['title'],
      $productArray['type'],
      $productArray['address'],
      $productArray['price'],
      $productArray['description'],
      $productArray['kitchen']
    )
  };
};

$writer = new HtmlProductsWriter();
            
  
?>






